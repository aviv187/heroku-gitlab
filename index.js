const express = require('express')
var cors = require('cors')
const app = express()
app.use(cors())
app.use(express.static("public", { maxAge: 60000 }));

const firebase = require('firebase')
var firebaseConfig = {
    apiKey: "AIzaSyBAZMnqS9M48dyS1BKp0WTKVDFwajMF5W0",
    authDomain: "irctest-d3f81.firebaseapp.com",
    projectId: "irctest-d3f81",
    storageBucket: "irctest-d3f81.appspot.com",
    messagingSenderId: "233428019570",
    appId: "1:233428019570:web:dd0bd23d33f6c15c8955eb"
};
firebase.initializeApp(firebaseConfig);

app.set('PORT', process.env.PORT || 3000)

// sending new book to firebase
app.get('/add', (req, res) => {
    const title = req.query.title
    const author = req.query.author
    const summary = req.query.summary
    const genre = req.query.genre
    const rating = req.query.rating

    firebase.firestore().collection("books").add({
        title: title,
        author: author,
        summary: summary,
        genre: genre,
        rating: rating,
    });

    res.status(200).send({ message: `Adding ${title}` });
});

//getting all the books from firebade
app.get('/get', (req, res) => {
    firebase.firestore().collection("books").get().then(snapshot => {
        let output = [];
        snapshot.forEach(doc => {
            output.push({ id: doc.id, ...doc.data() });
        })
        res.status(200).send(output);
    }).catch(e => {
        res.status(500).send(e);
    });
})

app.listen(app.get('PORT'), () =>
    console.log(`Server running on port ${app.get('PORT')}`),
)