# Heroku GitLab
Deploying to Heroku

## How to build a new flutter app
1. Clone flutter app from gitlab: https://gitlab.com/aviv187/mybook
2. Create build using 'flutter build web' (check out the README for more info)
3. Move build/web content to public/

## How to download docker:
1. docker pull aviv34/ironclic-node-server:latest
2. docker run -p 49160:3001 aviv34/ironclic-node-server

## How to use node:
1. get url + '/': call the public folder wich hold the web app
2. get url + '/get': call firestore to get all the books in the database
3. get url + '/add': call firestore to add a new book to the database